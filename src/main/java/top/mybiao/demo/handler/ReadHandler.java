package top.mybiao.demo.handler;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import top.mybiao.demo.config.RpcConfig;
import top.mybiao.demo.config.ServerConfig;
import top.mybiao.demo.dto.EmptyDto;
import top.mybiao.demo.dto.RpcProto;
import top.mybiao.demo.service.impl.MyServiceImpl;
import top.mybiao.demo.utils.SerialUtil;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.concurrent.*;

public class ReadHandler extends SimpleChannelInboundHandler<String> {

    private ServerConfig config;
    private ExecutorService executorService;

    public ReadHandler(ServerConfig config){
        this.config = config;
        int n = Runtime.getRuntime().availableProcessors();
        //初始化线程池
        executorService = new ThreadPoolExecutor(n,n*2,30, TimeUnit.SECONDS,new LinkedBlockingDeque<>());
    }

    protected void channelRead0(ChannelHandlerContext channelHandlerContext, String s)
            throws Exception {
        System.out.println("read-1");
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        executorService.submit(()->{
            ByteBuf buf=(ByteBuf)msg;
            byte[] bytes=new byte[buf.readableBytes()];
            buf.readBytes(bytes);
            RpcProto proto = (RpcProto) SerialUtil.unSerial(bytes);
            rpcDeal(ctx,proto);
        });
    }

    private void rpcDeal(ChannelHandlerContext ctx,RpcProto proto) {
        try {
            String name = proto.getCls().getName();
            var services = config.getRpc().getService();
            String reference = null;
            for (RpcConfig config : services) {
                if (config.getName().equals(name)) {
                    reference = config.getReference();
                }
            }
            String implClassName = reference;
            Class<?> clazz = Class.forName(implClassName);
            Object obj = clazz.getConstructor().newInstance();
            Method method = clazz.getMethod(proto.getMethodName(), proto.getParamType());
            Object res = method.invoke(obj, proto.getArgs());
            if (proto.getReturnType() == void.class || proto.getReturnType() == Void.class) {
                EmptyDto dto = new EmptyDto();
                byte[] buf = SerialUtil.serial(dto);
                //must write data
                ctx.writeAndFlush(Unpooled.copiedBuffer(buf));
                return;
            }
            byte[] buf = SerialUtil.serial(res);
            ctx.writeAndFlush(Unpooled.copiedBuffer(buf));
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
