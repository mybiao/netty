package top.mybiao.demo;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import top.mybiao.demo.config.MyConfig;
import top.mybiao.demo.config.ServerConfig;
import top.mybiao.demo.handler.ReadHandler;
import top.mybiao.demo.handler.StringHandler;

public class Server {

    private ServerConfig config;

    public Server(){
        MyConfig myConfig=new MyConfig();
        this.config = myConfig.parse();
    }

    public void start(){
        NioEventLoopGroup boss = new NioEventLoopGroup();
        NioEventLoopGroup work = new NioEventLoopGroup();
        ServerBootstrap bootstrap=new ServerBootstrap();
        bootstrap.group(boss,work)
                .channel(NioServerSocketChannel.class)
                .option(ChannelOption.SO_BACKLOG,1024)
                .childOption(ChannelOption.SO_KEEPALIVE,true)
                .childHandler(new ChannelInitializer<NioSocketChannel>() {
                    protected void initChannel(NioSocketChannel ch) throws Exception {
//                        ch.pipeline().addLast(new StringHandler());
                        ch.pipeline().addLast(new ReadHandler(config));
                    }
                });
        bootstrap.bind(8080).addListener(future->{
            if (future.isSuccess()){
                System.out.println("端口8080绑定成功");
            }else{
                System.out.println("端口未绑定");
            }
        });
    }




    public static void main(String[] args) {
        Server server=new Server();
        server.start();
    }
}
