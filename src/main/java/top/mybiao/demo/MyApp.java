package top.mybiao.demo;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import top.mybiao.demo.dto.RpcProto;
import top.mybiao.demo.utils.SerialUtil;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.concurrent.SynchronousQueue;

public class MyApp {

    private ChannelHandlerContext ctx;

    private SynchronousQueue<Object> queue;

    public MyApp(ChannelHandlerContext ctx, SynchronousQueue<Object> queue){
        this.ctx = ctx;
        this.queue=queue;
    }

    public Object sendSync(String msg) throws InterruptedException {
        ByteBuf buf = Unpooled.copiedBuffer(msg, StandardCharsets.UTF_8);
        this.ctx.writeAndFlush(buf);
        return queue.take();
    }

    public Object sendSync(ByteBuf buf) throws InterruptedException {
        this.ctx.writeAndFlush(buf);
        return queue.take();
    }

    @SuppressWarnings("unchecked")
    public <T> T  getObject(Class<T> cls){
        return (T)Proxy.newProxyInstance(this.getClass().getClassLoader(), new Class[]{cls}, (proxy, method, args) -> {
            Object res = null;
            Method []ms = cls.getMethods();
            for (Method m: ms){
                //遍历所有方法
                if (m.equals(method)){
                    RpcProto proto=getProto(cls,method,args);
                    byte[] buf = SerialUtil.serial(proto);
                    ByteBuf byteBuf = Unpooled.copiedBuffer(buf);
                    res = sendSync(byteBuf);
                }
            }
            //返回执行结果
            return res;
        });
    }

    private RpcProto getProto(Class<?> cls,Method m,Object[] args){
        RpcProto rpcProto=new RpcProto();
        rpcProto.setCls(cls);
        rpcProto.setArgs(args);
        rpcProto.setMethodName(m.getName());
        rpcProto.setParamType(m.getParameterTypes());
        rpcProto.setReturnType(m.getReturnType());
        return rpcProto;
    }

}
