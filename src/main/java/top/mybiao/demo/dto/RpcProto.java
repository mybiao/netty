package top.mybiao.demo.dto;

import java.io.Serializable;

public class RpcProto implements Serializable {
    private static final long serialVersionUID = -4459170068888073918L;
    private Class<?> cls;

    private String methodName;

    //参数列表
    private Object []args;

    //参数类型
    private Class<?> []paramType;

    private Class<?> returnType;

    public Class<?> getCls() {
        return cls;
    }

    public void setCls(Class<?> cls) {
        this.cls = cls;
    }

    public Object[] getArgs() {
        return args;
    }

    public void setArgs(Object[] args) {
        this.args = args;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public Class<?>[] getParamType() {
        return paramType;
    }

    public void setParamType(Class<?>[] paramType) {
        this.paramType = paramType;
    }

    public Class<?> getReturnType() {
        return returnType;
    }

    public void setReturnType(Class<?> returnType) {
        this.returnType = returnType;
    }
}
