package top.mybiao.demo.utils;

import java.io.*;

public class SerialUtil {

    public static Object unSerial(byte[] buf){
        try {
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(buf);
            ObjectInputStream in = new ObjectInputStream(byteArrayInputStream);
            return in.readObject();
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public static byte[] serial(Object obj) throws IOException {
        try {
            ByteArrayOutputStream buf = new ByteArrayOutputStream();
            ObjectOutputStream out = new ObjectOutputStream(buf);
            out.writeObject(obj);
            return buf.toByteArray();
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }
}
