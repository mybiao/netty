package top.mybiao.demo.config;

import java.util.List;

public class ServiceProperties {

    List<RpcConfig> service;

    public List<RpcConfig> getService() {
        return service;
    }

    public void setService(List<RpcConfig> service) {
        this.service = service;
    }
}
