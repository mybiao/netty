package top.mybiao.demo.config;

import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;

import java.io.InputStream;

public class MyConfig {

    public ServerConfig parse(){
        Yaml yaml=new Yaml(new Constructor(ServerConfig.class));
        InputStream inputStream=this.getClass().getClassLoader().getResourceAsStream("properties.yml");
        return yaml.load(inputStream);
    }
}
