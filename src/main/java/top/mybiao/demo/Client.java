package top.mybiao.demo;

import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import top.mybiao.demo.handler.ClientScanOutHandler;
import top.mybiao.demo.handler.StringHandler;
import top.mybiao.demo.service.MyService;

import java.nio.charset.StandardCharsets;
import java.util.List;

public class Client {

    private ChannelFuture future;

    private ClientHandler clientHandler=new ClientHandler();

    public void connect() throws InterruptedException {
        NioEventLoopGroup group=new NioEventLoopGroup();
        Bootstrap bootstrap=new Bootstrap();
        bootstrap.group(group)
                .channel(NioSocketChannel.class)
                .handler(new ChannelInitializer<NioSocketChannel>() {
                    @Override
                    protected void initChannel(NioSocketChannel ch) throws Exception {
                        ch.pipeline().addLast(new ClientScanOutHandler());
//                        ch.pipeline().addLast(new StringHandler());
                        ch.pipeline().addLast(clientHandler);
                    }
                });
        future = bootstrap.connect("localhost",8080);
    }

    public void send(String msg){
        ByteBuf buf = Unpooled.copiedBuffer(msg, StandardCharsets.UTF_8);
        var f = future.channel().writeAndFlush(buf);
    }

    public MyApp initApp() throws InterruptedException {
        return clientHandler.initApp();
    }


    public static void main(String[] args) throws InterruptedException {
        Client client=new Client();
        client.connect();
        MyApp app=client.initApp();
        MyService myService=app.getObject(MyService.class);
        myService.say("trump",20);

        String s = myService.complex(List.of("one","two","three"));
        System.out.println(s);
    }
}
