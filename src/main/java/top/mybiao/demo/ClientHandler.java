package top.mybiao.demo;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import top.mybiao.demo.utils.SerialUtil;

import java.util.concurrent.SynchronousQueue;

public class ClientHandler extends SimpleChannelInboundHandler<String> {

    private ChannelHandlerContext ctx;

    private final SynchronousQueue<Object> queue=new SynchronousQueue<>();


    @Override
    protected void channelRead0(ChannelHandlerContext channelHandlerContext, String s) throws Exception {
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        this.ctx = ctx;
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        ByteBuf buf = (ByteBuf)msg;
        byte []bytes=new byte[buf.readableBytes()];
        buf.readBytes(bytes);
        Object res= SerialUtil.unSerial(bytes);
        queue.offer(res);
    }

    public synchronized MyApp initApp() throws InterruptedException {
        while (this.ctx==null){
            wait(10);
        }
        return new MyApp(this.ctx, this.queue);
    }
}
