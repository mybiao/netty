package top.mybiao.demo.service;

import java.util.List;

public interface MyService {

    String hello(String name);

    void say(String name,int age);

    String complex(List<String> list);
}
