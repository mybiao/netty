package top.mybiao.demo.service.impl;

import top.mybiao.demo.service.MyService;

import java.util.List;

public class MyServiceImpl implements MyService {
    @Override
    public String hello(String name) {
        return "welcome "+name;
    }

    @Override
    public void say(String name, int age) {
        System.out.println(name+" say I'm "+age+" years old");
    }

    @Override
    public String complex(List<String> list) {
        System.out.println(list);
        return "receive ok";
    }
}
